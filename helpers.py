from enum import Enum
import yaml

class State(Enum):
    IDLE        = 1
    MOVING      = 2
    TRANSFERING = 3
    BROKEN      = 4
    
class Colors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKRARO= '\033[97m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    
class EventType(Enum):
    simulation_start = 0
    #Elevator events
    call_elevator    = 1
    finish_travel    = 2
    close_doors      = 3
    fix_elevator     = 4
    #Generator events
    new_arrival      = 5
    #Stairs events
    get_stairs       = 6
    #Floor events
    transfer_people  = 7
    #Person events
    finish_work      = 8
    
class Parser():
    def load_values(path):
        with open(path, 'r') as stream:
            try:
                return yaml.safe_load(stream)
            except yaml.YAMLError as exc:
                print("Error:", exc, "\nal convertir",path)
                exit()