import sys, bisect
from entities.elevator import Elevator
from entities.floor import Floor
from token_generator import TokenGenerator
from event import *
from helpers import Parser

values_path = './values.yaml'
if len(sys.argv) > 1:
    values_path = sys.argv[1]

class Scheduler:

    currentTime = 0
    all_events = []
    
    def __init__(self):    
        self.start_simulation = Event(self, self, 0, EventType.simulation_start)
        self.all_events.append(self.start_simulation)
        self.yaml_values = Parser.load_values(values_path).get('values')
        self.simulation_time = self.yaml_values.get('simulation_time')
        self.number_floors = self.yaml_values.get('number_floors')
        self.min_arrival_time = self.yaml_values.get('arrival').get('min')
        self.max_arrival_time = self.yaml_values.get('arrival').get('max')
        self.min_stay_time = self.yaml_values.get('person_stay_time').get('min')
        self.max_stay_time = self.yaml_values.get('person_stay_time').get('max')
        self.events_view = 0
        self.elevator_queue = []
        self.stairs_uses = 0
 
    def run(self):
        self.configurate()
        self.create_models()
    
        self.current_time = 0        
        while self.current_time < self.simulation_time:
            event = self.all_events.pop(0)
            self.current_time = event.time
            if self.events_view == 1:
                print(Colors.WARNING, "Se va a procesar un evento de tipo", event.type, Colors.ENDC)
            event.object.process_event(event)

        print(Colors.HEADER, "ESTADÍSTICOS", Colors.ENDC)
        print("Personas generadas en el sistema:", self.generator.created_entities)

        print(Colors.OKGREEN, "Ascensor 1", Colors.ENDC)
        print("     Servicios totales:", self.elevator1.total_services)
        print("     Número de veces que se ha estropeado:", self.elevator1.total_breaks, )
        print("     Personas actuales en el ascensor:", len(self.elevator1.a_persons))

        print(Colors.OKGREEN, "Ascensor 2", Colors.ENDC)
        print("     Servicios totales:", self.elevator2.total_services)
        print("     Número de veces que se ha estropeado:", self.elevator2.total_breaks, )
        print("     Personas actuales en el ascensor:", len(self.elevator2.a_persons))

        print(Colors.OKGREEN, "Ascensor 3", Colors.ENDC)
        print("     Servicios totales:", self.elevator3.total_services)
        print("     Número de veces que se ha estropeado:", self.elevator3.total_breaks, )
        print("     Personas actuales en el ascensor:", len(self.elevator3.a_persons))

        print(Colors.OKGREEN, "Escaleras", Colors.ENDC)
        print("     Veces que se han usado las escaleras:", self.stairs_uses)
        
        print(Colors.OKGREEN, "Pisos", Colors.ENDC)
        for floor in self.floor_list:
            if floor.num_floor != 0:
                print("     En el piso", floor.num_floor, "hay", floor.number_entities, "personas")

        print("Llamadas pendientes a ascensor:", len(self.elevator_queue))
        print("Eventos de la simulación en cola:", len(self.all_events))
        
    def configurate(self):
        print('-----------Simulación a medida de ascensores-----------')
        error = True
        while error:
            self.execution_type = (int)(input("Quieres ejecutar la simulación de manera manual (1) o automática (2)?: "))
            if self.execution_type == 1:
                self.simulation_time = (int)(input("Tiempo de simulación: "))
                self.number_floors = (int)(input("Número de plantas del edificio: "))
                print("Tiempos de llegada de personas al edificio")
                self.max_arrival_time = (int)(input("Tiempo máximo: "))
                self.min_arrival_time = (int)(input("Tiempo mínimo: "))
                print("Tiempos de estancia de la persona en el edificio")
                self.max_stay_time = (int)(input("Tiempo máximo: "))
                self.min_stay_time = (int)(input("Tiempo mínimo: "))
                self.events_view = (int)(input("Visualizar registro de eventos Si [1], No [0]: "))                
                error = False
            elif self.execution_type == 2:
                error = False
            else:
                print("Introduce un parámetro válido !")
                
    def create_models(self):
        elvs = self.yaml_values.get('elevators')
        self.generator = TokenGenerator(self, self.min_arrival_time, self.max_arrival_time, self.min_stay_time, self.max_stay_time)
        self.elevator1 = Elevator(elvs.get('capacity'), elvs.get('travel_time'), elvs.get('fail_1'), elvs.get('repair_time'), elvs.get('transfer_time'), self, 1)
        self.elevator2 = Elevator(elvs.get('capacity'), elvs.get('travel_time'), elvs.get('fail_2'), elvs.get('repair_time'), elvs.get('transfer_time'), self, 2)
        self.elevator3 = Elevator(elvs.get('capacity'), elvs.get('travel_time'), elvs.get('fail_3'), elvs.get('repair_time'), elvs.get('transfer_time'), self, 3)
        
        self.floor_list = []
        self.floor_list.append(Floor(0, self))
        for floor_id in range(self.number_floors):
            self.floor_list.append(Floor(floor_id + 1, self))
            
        self.generator.connections(self.elevator1, self.elevator2, self.elevator3)

    def add_event(self, event):
        bisect.insort(self.all_events, event)
        
    def process_event(self, event):
        if (event.type == EventType.simulation_start):
            #initialize all classes
            self.generator.start()

if __name__ == "__main__":
    scheduler = Scheduler()
    scheduler.run()
