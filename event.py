from helpers import *

class Event:
    def __init__(self,
                 event_creator,
                 object,
                 time,
                 type):
        self.creator = event_creator
        self.object = object
        self.time = time
        self.type = type
        
    def __repr__(self):
        return str(self.time)+' '+str(self.type)
    
    def __gt__(self, event):
        return self.time > event.time