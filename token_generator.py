import random
from helpers import *
from entities.person import Person
from event import Event

class TokenGenerator:

    def __init__(self,scheduler, min_time, max_time, min_stay_time, max_stay_time):
        self.min_time = min_time
        self.max_time = max_time
        self.min_stay_time = min_stay_time
        self.max_stay_time = max_stay_time
        #Variables temporales
        self.scheduler = scheduler
        self.created_entities = 0
        self.transport = []

    def __repr__(self):
        return "Generator"

    def start(self):
        self.next_arrival(0)
        
    def connections(self,elevator1,elevator2,elevator3):
        self.elevator1 = elevator1
        self.elevator2 = elevator2
        self.elevator3 = elevator3
        self.transport = [self.elevator1, self.elevator2, self.elevator3]
        
    def process_event(self, event):
        if (event.type == EventType.new_arrival):
            self.process_next_arrival(event)
        
    def next_arrival(self, temps):
        time_between_arrivals = random.uniform(self.min_time, self.max_time)
        self.scheduler.add_event(Event(self, self, temps + time_between_arrivals, EventType.new_arrival))
        
    def process_next_arrival(self, event):
        self.next_arrival(event.time)
        self.created_entities = self.created_entities + 1
        new_person = Person(self.created_entities, self.scheduler.number_floors, random.uniform(self.min_stay_time, self.max_stay_time), self.scheduler)

        if self.scheduler.events_view == 1:
            print("Se ha generado la persona #", self.created_entities, "con destino: planta", new_person.person_destination)
        
        if new_person.person_destination % 2 == 0:
            new_person.available_transports = [self.elevator1, self.elevator3]
        else: 
            new_person.available_transports = [self.elevator2, self.elevator3]

        new_person.start(event)
        