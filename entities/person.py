import random
from helpers import *
from event import *

class Person():
    
    def __init__(self, id, num_floors, work_time, scheduler):
        self.id = id
        self.person_destination = (int)(random.uniform(1, num_floors + 1))
        self.work_time = work_time
        #Relaciones de clases
        self.available_transports = []
        #Variables temporales
        self.scheduler = scheduler
        self.current_floor = 0
        
    def start(self, event):
        self.get_transport(event)
        self.scheduler.add_event(Event(self, self, event.time + self.work_time, EventType.finish_work))

    def process_event(self, event):
        if event.type == EventType.finish_work:
            self.current_floor = self.person_destination
            self.person_destination = 0
            self.get_transport(event)

    def get_transport(self, event):
        if self.available_transports[0].available:
            if self.available_transports[0].state == State.IDLE:
                self.available_transports[0].transport_destination = self.current_floor
                self.scheduler.add_event(Event(self, self.available_transports[0], event.time, EventType.call_elevator))
            else:
                self.scheduler.elevator_queue.append(Event(self, self.available_transports[0], event.time, EventType.call_elevator))

        elif self.available_transports[1].available:            
            if self.available_transports[1].state == State.IDLE:
                self.available_transports[1].transport_destination = self.current_floor
                self.scheduler.add_event(Event(self, self.available_transports[1], event.time, EventType.call_elevator))
            else:
                self.scheduler.elevator_queue.append(Event(self, self.available_transports[1], event.time, EventType.call_elevator))

        else:
            if self.scheduler.events_view == 1:
                print("La persona #", self.id, "ha utilizado las escaleras para ir a la planta", self.person_destination, "ya que no había ascensores disponibles")
            self.scheduler.stairs_uses = self.scheduler.stairs_uses + 1