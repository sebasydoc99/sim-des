from helpers import *
from event import Event

class Floor():
    
    def __init__(self, num_floor, scheduler):
        self.num_floor = num_floor
        self.scheduler = scheduler
        self.number_entities = 0
        
    def __str__(self):
        return str(self.num_floor)
        
    def process_event(self, event):
        if event.type == EventType.transfer_people:
            self.number_entities = self.number_entities + event.creator.persons_to_floor - len(event.creator.a_persons)
            if self.scheduler.events_view == 1:
                print("Del ascensor", event.creator.type, "han bajado", event.creator.persons_to_floor, "al piso", self.num_floor, "y han subido", len(event.creator.a_persons), "personas")
            self.scheduler.add_event(Event(self, event.creator, event.time, EventType.close_doors))