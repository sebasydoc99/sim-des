from helpers import *
from event import *

class Elevator():
    
    def __init__(self, capacity, travel_time, max_services, repair_time, transfer_time, scheduler, type):
        self.travel_time = travel_time
        self.capacity = capacity
        self.available = True
        self.max_services = max_services
        self.repair_time = repair_time
        self.type = type
        self.transfer_time = transfer_time
        #Relaciones de la clase
        self.state = State.IDLE
        self.a_persons = []
        #Variables temporales
        self.scheduler = scheduler
        self.current_services = 0
        self.current_floor = 0
        self.total_services = 0
        self.total_breaks = 0
        self.transport_destination = 0
        self.persons_to_floor = 0
        self.caller = 0
        
    def process_event(self, event):
        if event.type == EventType.call_elevator:
            self.state = State.MOVING
            if self.current_floor != self.transport_destination:
                self.current_services = self.current_services + 1
                self.total_services = self.total_services + 1
            t_time = abs(self.current_floor - self.transport_destination) * self.travel_time
            self.caller = event.creator
            self.scheduler.add_event(Event(self, self, event.time + t_time, EventType.finish_travel))
            if self.scheduler.events_view == 1:
                print(Colors.OKBLUE, "El ascensor", self.type, "ha sido llamado desde el piso", self.transport_destination, "y está yendo desde el piso", self.current_floor, ". State = MOVING", Colors.ENDC)
        
        elif event.type == EventType.finish_travel:         
            self.state = State.TRANSFERING
            self.current_floor = self.transport_destination
            self.transfer(self.caller, self.scheduler.elevator_queue, event)
            if self.scheduler.events_view == 1:
                print(Colors.OKBLUE, "El ascensor", self.type, "ha llegado al piso", self.current_floor, "con", self.current_services, " servicios. State = TRANSFERING", Colors.ENDC)

            self.scheduler.add_event(Event(self, self.scheduler.floor_list[self.transport_destination], event.time + self.transfer_time, EventType.transfer_people))
            
        
        elif event.type == EventType.close_doors:
            if self.current_services >= self.max_services:
                self.total_breaks = self.total_breaks + 1
                self.state = State.BROKEN
                self.available = False
                self.scheduler.add_event(Event(self, self, event.time + self.repair_time, EventType.fix_elevator))
                if self.scheduler.events_view == 1:
                    print(Colors.OKBLUE, "El ascensor", self.type, "se ha estropeado después de ", self.current_services, "servicios. State = BROKEN", Colors.ENDC)
                
                if self.current_floor != self.caller.person_destination:
                    if self.scheduler.events_view == 1:
                        print("El ascensor", self.type, "se ha roto antes de que la persona llegase a su destino. La persona realiza otra llamada")
                    self.caller.get_transport(event)
            elif len(self.a_persons) == 0:
                self.state = State.IDLE
                if self.scheduler.events_view == 1:
                    print(Colors.OKBLUE, "El ascensor", self.type, "está libre en la planta", self.current_floor, ". State = IDLE", Colors.ENDC)

                next_event = self.get_next_event()
                if next_event != 0:
                    if self.scheduler.events_view == 1:
                        print("El ascensor", self.type, "tiene llamadas pendientes")
                    next_event.creator.get_transport(event)
            else:
                self.state = State.MOVING
                self.current_services = self.current_services + 1
                self.total_services = self.total_services + 1
                self.transport_destination = self.caller.person_destination
                t_time = abs(self.current_floor - self.transport_destination) * self.travel_time
                if self.scheduler.events_view == 1:
                    print(Colors.OKBLUE, "El ascensor", self.type, "está yendo del piso", self.current_floor, "al piso", self.transport_destination, "con ", len(self.a_persons), "personas. State = MOVING", Colors.ENDC)
                self.scheduler.add_event(Event(self, self, event.time + t_time, EventType.finish_travel))
        
        elif event.type == EventType.fix_elevator:
            self.repair(event)
        
    def transfer(self, current_person, event_queue, event):
        if self.current_floor == current_person.person_destination:
            self.persons_to_floor = len(self.a_persons)
            self.a_persons = []
            if self.scheduler.events_view == 1:
                print("El ascensor", self.type, "se ha vaciado en el piso", self.current_floor)
        else:
            if self.current_services < self.max_services:
                self.persons_to_floor = 0
                self.a_persons = [current_person]
                for event in event_queue:
                    if event.creator.current_floor == current_person.current_floor and event.creator.person_destination == current_person.person_destination and len(self.a_persons) < self.capacity:
                        self.a_persons.append(event.creator)
                        event_queue.remove(event)
                    elif len(self.a_persons) >= self.capacity:
                        break
                if self.scheduler.events_view == 1:
                    print("En el ascensor", self.type, "han entrado", len(self.a_persons), "personas en el piso", self.current_floor)
            else:
                self.scheduler.elevator_queue.append(Event(current_person, self, event.time, EventType.call_elevator))

    def repair(self, event):
        self.state = State.IDLE
        self.available = True
        self.current_services = 0
        next_event = self.get_next_event()
        if next_event != 0:
            if self.scheduler.events_view == 1:
                print("El ascensor", self.type, "tiene llamadas pendientes")
            next_event.creator.get_transport(event)

    def get_next_event(self):
        for event in self.scheduler.elevator_queue:
            if event.object.type == self.type:
                self.scheduler.elevator_queue.remove(event)
                return event
            elif self.type == 3 and (event.object.type != 3 and not event.object.available):
                self.scheduler.elevator_queue.remove(event)
                return event
            elif self.type != 3 and event.object.type == 3 and self.type == event.creator.available_transports[0]:
                self.scheduler.elevator_queue.remove(event)
                return event
        return 0
