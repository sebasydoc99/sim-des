# README #

### To download dependencies ###

* First of all create a virtual environment
```
$ python3 -m venv ./venv
$ source venv/bin/activate
$ pip install -r requirements.txt
```
* Now you will have your dependencies on your virtual environment
* To end with the venv execute next command
```
$ deactivate
```

* Every time you download a new dependency, remember to add it to the requirements.txt
```
$ pip install .....
$ pip freeze > requirements.txt
```